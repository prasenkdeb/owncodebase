<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Auth;
use DB;
use Input;
use Carbon\Carbon;
use Config;
use App\{User,QuickBlox,Udid,UserGeneralInfo,ScoutMaster,GrowerFpoModel,User_step,Master_srvcduration,UserOrders,UserPlots,Master_forecast,UserLocation};
use Redirect;
use Razorpay\Api\Api;
use App\Helper\Helper;

class UserController extends Controller
{
    //

    public function profileDetail(Request $requests)
    {
    	
        $input = $requests->all();

        $usr_id = $requests->user()->usr_id;

        $getUser = User::with('getGrowerGeneralInfo','userLocation','userCropDetails','walletDetails','growerFpo.fetch_channel','userGetQuickblox')
                    ->where('usr_id', $usr_id)->first();

        $userStepData= array();
    	if(sizeof($requests->user()->userStepData)>0){

    		$stp_cropNewVal= $requests->user()->userStepData->stp_service;

    		if($requests->user()->userStepData->stp_service== 'y'){
    			$getUserPlot = UserPlots::where('plt_usr_id', $usr_id)->orderBy('plt_id', 'DESC')->first();
    			if(!empty($getUserPlot)){
    				$date_now = date("Y-m-d");
    				$plt_active_to= $getUserPlot->plt_active_to;

    				if ($date_now > $plt_active_to) {
					    $stp_cropNewVal= 'n';
					}
    			}
    		}

    		$userStepData= array(
    			'stp_crop' => $requests->user()->userStepData->stp_crop,
    			'stp_service' => $stp_cropNewVal,
    			//'stp_service' => $requests->user()->userStepData->stp_service,
    		);
    	}

        if ($getUser) {

            return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Data Fetched', 'userStepData'=>$userStepData, 'data'=>$getUser]); 

        }else{
        	return response()->json(['status'=>false, 'status_code'=>401,'message'=>'No User Found'], 400);
        }
    }
	
	public function updateDetail(Request $requests)
    {
    	$usr_data= $requests->user();
		$user_id=$usr_data->usr_id;
		$user_type=$usr_data->usr_type_id;
		$usr_id = $usr_data->usr_id;
		$user_email=$usr_data->usr_email;
		
        $input = $requests->all();

		
		$new_file_name='';
				
				
		if($requests->mode=='0')
		{
			if($user_email=='')
			{
					$validator = Validator::make($requests->all(), [
				   
					'usr_email' => 'email',
					'alternate_number' => 'numeric|digits:10',         
					
							]);

					 if ($validator->fails()) {

						return response()->json(['status'=>false, 'status_code'=>401,'message'=>$validator->messages()->first()], 400);
					}
			
				
			$vvu=User::where('usr_id',$usr_id)->where('usr_type_id',$user_type)->update(['usr_email'=>$requests->usr_email ]);
			
			//$vv=UserGeneralInfo::where('gen_usr_id',$usr_id)->where('gen_usr_type_id',$user_type)->update(['first_name'=>$requests->f_name,'last_name'=>$requests->l_name,'gender'=>$requests->gender,'dob'=>$requests->dob,'user_type'=>$requests->user_type,'alternate_number'=>$requests->alternate_number,'language'=>$requests->language ]);
			
			//return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Data update successfully']); 
			}
			
			else if($user_email!=$requests->usr_email)
			{
					$validator = Validator::make($requests->all(), [
				   
					'usr_email' => 'email|unique:fn_user',
					'alternate_number' => 'numeric|digits:10',         
					
							]);

					 if ($validator->fails()) {

						return response()->json(['status'=>false, 'status_code'=>401,'message'=>$validator->messages()->first()], 400);
					}
			
				
			$vvu=User::where('usr_id',$usr_id)->where('usr_type_id',$user_type)->update(['usr_email'=>$requests->usr_email ]);
			
			//$vv=UserGeneralInfo::where('gen_usr_id',$usr_id)->where('gen_usr_type_id',$user_type)->update(['first_name'=>$requests->f_name,'last_name'=>$requests->l_name,'gender'=>$requests->gender,'dob'=>$requests->dob,'user_type'=>$requests->user_type,'alternate_number'=>$requests->alternate_number,'language'=>$requests->language ]);
			
			 
			}
			else{
				$vvu=User::where('usr_id',$usr_id)->where('usr_type_id',$user_type)->update(['usr_email'=>$requests->usr_email ]);
			
			}
			
			
			
			$vv=UserGeneralInfo::where('gen_usr_id',$usr_id)->where('gen_usr_type_id',$user_type)->update(['first_name'=>$requests->f_name,'last_name'=>$requests->l_name,'gender'=>$requests->gender,'dob'=>$requests->dob,'user_type'=>$requests->user_type,'alternate_number'=>$requests->alternate_number,'language'=>$requests->language ]);
			
			return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Data update successfully']);
		}
		else if($requests->mode=='1')
		{
			
			if($requests->hasFile('prof_img'))
				{	
					$file = $requests->file('prof_img');
					//$imageName = rand(1111,9999).'_'.time().$userId.'.png';
					
					//$new_file_name = $user_id.'_'.$file_name;
					$file_name = $file->getClientOriginalName();
					$file_extension = $file->getClientOriginalExtension();

					//$user_id = $getUser->id;
					
					$new_file_name = rand(1111,9999).'_'.time().$usr_id.'.png';

					$path = Config::get('constants.IMAGE_PATH');

					$destination_path = $path.'/user';
					// if(!is_dir($destination_path))
					// {
						// mkdir($destination_path);
					// }
					// chmod($destination_path,0777);
					$file->move($destination_path, $new_file_name);
				}
			
			$vv=UserGeneralInfo::where('gen_usr_id',$usr_id)->where('gen_usr_type_id',$user_type)->update(['prof_img'=>$new_file_name ]);
			
			return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Profile image upload successfully']); 
		}
		else{
			return response()->json(['success'=>false,'status_code'=>400]);
		}

        // $getUser = User::with('getGrowerGeneralInfo','userLocation','userCropDetails','walletDetails','growerFpo.fetch_channel')
                    // ->where('usr_id', $usr_id)->first();

      // return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Data update successfully']); 
    }
	
	public function updateImage(Request $requests)
    {
    	
        $input = $requests->all();

        $usr_id = $requests->user()->usr_id;
		
				
		$new_file_name='';
				if($requests->hasFile('prof_img'))
				{	
					$file = $requests->file('prof_img');
					//$imageName = rand(1111,9999).'_'.time().$userId.'.png';
					
					//$new_file_name = $user_id.'_'.$file_name;
					$file_name = $file->getClientOriginalName();
					$file_extension = $file->getClientOriginalExtension();

					//$user_id = $getUser->id;
					
					$new_file_name = rand(1111,9999).'_'.time().$usr_id.'.png';

					$path = Config::get('constants.IMAGE_PATH');

					$destination_path = $path.'/user';
					// if(!is_dir($destination_path))
					// {
						// mkdir($destination_path);
					// }
					// chmod($destination_path,0777);
					$file->move($destination_path, $new_file_name);
					
					$vv=UserGeneralInfo::where('gen_usr_id',$usr_id)->update(['prof_img'=>$new_file_name ]);
				}
				
		

       return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Data update successfully']); 
    }


    public function registerCrop(Request $requests){
    	$usr_id = $requests->user()->usr_id;

    	$input = $requests->all();

    	$plt_service_type_req ='';
    	$no_of_plot_req= '';
    	$showing_date_req= '';
    	$land_areae_req= '';
    	$area_unit_req= '';
    	$irrigation_avl_req= '';

    	if(sizeof($requests->user()->userStepData)>0){

	    	if($requests->user()->userStepData->stp_crop=='y'){
		    	$plt_service_type_req ='';
		    	$no_of_plot_req= '';
		    	$showing_date_req= '';
		    	$land_areae_req= '';
		    	$area_unit_req= '';
		    	$irrigation_avl_req= '';
		    }else{
		    	$plt_service_type_req= 'required';
		    	$no_of_plot_req= 'required|numeric';
		    	$showing_date_req= 'required';
		    	$land_areae_req= 'required';
		    	$area_unit_req= 'required';
		    	$irrigation_avl_req= 'required';
		    }
		}


        $messages = [
            'crop_id.required' => 'Crop Field is Required',
            // 'plt_service_type.required' => 'Plot Service Type is Required',
            // 'no_of_plot.required' => 'No of Plot is Required',
            // 'showing_date.required' => 'Showing Date is Required',
            // 'land_area.required' => 'Land Area is Required',
            // 'area_unit.required' => 'Area Unit is Required',
            // 'irrigation_avl.required' => 'Irrigation Avl is Required'
            ];

        $validator = Validator::make($input, [
            'crop_id' => 'required',
            'plt_service_type' => $plt_service_type_req,
            'no_of_plot' => $no_of_plot_req,
            'showing_date' => $showing_date_req,
            'land_area' => $land_areae_req,
            'area_unit' => $area_unit_req,
            'irrigation_avl' => $irrigation_avl_req,
            
        ], $messages);
    
        if ($validator->fails()) {
            return response()->json(['status'=>false, 'status_code'=>401,'message'=>$validator->messages()->first()], 400);
        }




    	$crop_id= $requests->crop_id;
    	$plt_service_type= $requests->plt_service_type;
    	$no_of_plot= $requests->no_of_plot;
    	$showing_date= $requests->showing_date;
    	$land_area= $requests->land_area;
    	$area_unit= $requests->area_unit;
    	$irrigation_avl= $requests->irrigation_avl;
    	$scout_id= $requests->scout_id;



    	if(sizeof($requests->user()->userStepData)>0){

	    	if($requests->user()->userStepData->stp_crop=='y'){
	    		$scout_id= $requests->user()->scout_id;
	    	}
	    }

	    if($scout_id == '' || $scout_id == NULL){
	    	$scout_id= 'KH100049';
	    }

	    //return response()->json(['status'=>false, 'status_code'=>401,'message'=>$scout_id], 400);

    	$land_area_expl= explode(',', $land_area);
    	$area_unit_expl= explode(',', $area_unit);
    	$showing_date_expl= explode(',', $showing_date);
    	$irrigation_avl_expl= explode(',', $irrigation_avl);

    	$fpo_id=0;



 	   	if(isset($scout_id)){
    		$dataScout = ScoutMaster::select('id', 'fpo_id')
    						
    						->with(['fpoMasterDet' => function($query){
							    $query->select(['id', 'fpo_unique_id', 'name']);
							}])
    						->where('scout_id',$scout_id)
							->first();

			if(sizeof($dataScout)>0){

				// $fpoGrower = array(
				// 			'user_id' => $usr_id,
				// 			'fpo_id' => $dataScout->fpo_id,
				// 			'created_at' => date('Y-m-d H:i:s'),
				// 		);
				// DB::table('fn_grower_fpo')->insert($fpoGrower);

				//
				$storeGrowerFpoModel = GrowerFpoModel::updateOrCreate(
					[
						'user_id'=>$usr_id,
					],
					[
						'fpo_id'=> $dataScout->fpo_id,
						'created_at'=>date('Y-m-d H:i:s')
					]
				);

				//

				$update_user = User::where('usr_id', $usr_id)->update([
	                'scout_id' => $scout_id
	            ]);
	            
				$fpo_id= $dataScout->fpo_id;

			}else{
				return response()->json(['status'=>false, 'status_code'=>401,'message'=>'Invalid Scout ID'], 400);
			}
    	}

    	$userStepData= array();
    	if(sizeof($requests->user()->userStepData)>0){

    		$userStepData= array(
    			'stp_crop' => $requests->user()->userStepData->stp_crop,
    			'stp_service' => $requests->user()->userStepData->stp_service,
    			//'stp_service' => $requests->user()->userStepData->stp_service,
    		);
    	}


    	$usr_id_chck = $requests->user()->usr_id;
    	$getUserPlot = UserPlots::where('plt_usr_id', $usr_id_chck)->orderBy('plt_id', 'DESC')->first();

    	

    	// get servc duration start

    	if($fpo_id !=0){

    		$getMasterSrvcDuration = Master_srvcduration::
    				where('fpo_id', $fpo_id)
    				->where('dur_crop_id', $crop_id)
                    ->where('dur_status', 'a');
            if(!empty($getUserPlot) && $requests->user()->userStepData->stp_service== 'y'){
				$getMasterSrvcDuration = $getMasterSrvcDuration->where('free_trial',  '!=' ,1);
			}

            $getMasterSrvcDuration = $getMasterSrvcDuration->orderBy('dur_period', 'ASC')
                    ->get(); 
    	}else{
    		$getMasterSrvcDuration = Master_srvcduration::
    			where('fpo_id', 0)
    			->where('dur_crop_id', $crop_id)
    			->where('dur_status', 'a');

    		if(!empty($getUserPlot) && $requests->user()->userStepData->stp_service== 'y'){
				$getMasterSrvcDuration = $getMasterSrvcDuration->where('free_trial',  '!=' ,1);
			}

    		$getMasterSrvcDuration = 	$getMasterSrvcDuration->orderBy('dur_period', 'ASC')
                ->get();
    	}

    	$srvc_data= array();

    	if(sizeof($getMasterSrvcDuration)>0){
    		foreach ($getMasterSrvcDuration as $key => $value) {

    			

    			if($plt_service_type=='M'){
    				$srvc_prc= 'Rs. '.$value->dur_price_multi;

    				$discount= $value->discount_multi;
    				$srvc_prc_ori= $value->dur_price_multi - $discount;
    			}else{
    				$srvc_prc= 'Rs. '.$value->dur_price;

    				$discount= $value->discount;
    				$srvc_prc_ori= $value->dur_price - $discount;
    			}

    			if($value->dur_period < 1){
    				$dur_var= $value->dur_period_days.' Days Trial';
    				$srvc_prc='Free';
    			}else{
    				$dur_var= $value->dur_period.' Month';
    			}

    			$srvc_data[]= array(
    				'dur_id' => $value->dur_id,
    				'dur_crop_id' => $value->dur_crop_id,
    				'dur_period' => $dur_var,
    				'dur_price' => $srvc_prc,
    				'discount' => $discount,
    				'dur_price_ori' => $srvc_prc_ori,
    			);
    		}
    	}else{
    		return response()->json(['status'=>false, 'status_code'=>401,'message'=>'No Subscription Data Available'], 400);
    	}

    	if(sizeof($requests->user()->userStepData)>0){

	    	if($requests->user()->userStepData->stp_service!='n'){
		    	return response()->json(['status'=>false, 'status_code'=>401,'message'=>'Subscription already added.'], 400);
		    }
		}


    	// get servc duration end
		$usr_loc_id = 0;
    	if(sizeof($requests->user()->userLocation)>0){
    		$usr_loc_id = $requests->user()->userLocation->loc_id;
    	}

    	if(sizeof($requests->user()->userStepData)>0){

	    	if($requests->user()->userStepData->stp_crop=='n'){

			    if($no_of_plot>0){
					for ($i=0; $i < $no_of_plot; $i++) { 
						$plotData = array(
							'plt_id' => '',
							'plt_loctn_id' => $usr_loc_id,
							'plt_usr_id' => $usr_id,
							'plt_name' => '',
							'plt_area' => $land_area_expl[$i],
							'plt_area_unit' => $area_unit_expl[$i],
							'plt_crop_id' => $crop_id,
							//'plt_crop_variety' => NULL,
							'plt_crop_sowing' => $showing_date_expl[$i],
							'plt_active_from' => '0000-00-00',
							'plt_active_to' => '0000-00-00',
							'plt_status' => 'a',
							'plt_order' =>  0,
							'irrigation' =>  $irrigation_avl_expl[$i],
							'plt_service_type' =>  $plt_service_type,
						);

						DB::table('fn_user_plots')->insert($plotData);
		                //$inserted_forecast_id = DB::getPdo()->lastInsertId();
					}

					$update_user_step = User_step::where('stp_user_id', $usr_id)->update([
		                'stp_crop' => 'y'
		            ]);

				}else{
					return response()->json(['status'=>false, 'status_code'=>401,'message'=>'Invalid Tot Plot No.'], 400);
				}
			}else{
				return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Crop and Plot already added.',  'getMasterSrvcDuration'=>$srvc_data]);
			}
		}else{
			return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Steps Not added.',  'getMasterSrvcDuration'=>$srvc_data]);
		}
		


		return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Data Fetched', 'getMasterSrvcDuration'=>$srvc_data]); 

    	
    }


    public function genSubscription(Request $requests){
    	$usr_id = $requests->user()->usr_id;

    	$input = $requests->all();


        $messages = [
            'dur_id.required' => 'Duration ID Field is Required',
            ];

        $validator = Validator::make($input, [
            'dur_id' => 'required|numeric',
            'dur_price_ori' => 'required',
            
        ], $messages);
    
        if ($validator->fails()) {
            return response()->json(['status'=>false, 'status_code'=>401,'message'=>$validator->messages()->first()], 400);
        }

    	$dur_id= $requests->dur_id;
    	$dur_price_ori= $requests->dur_price_ori;
    	$dur_crop_id= 0;
    	$dur_period= 0;

    	$getMasterSrvcDuration = Master_srvcduration::
    				where('dur_id', $dur_id)
                    ->first();

        if(sizeof($getMasterSrvcDuration)>0){
    		$dur_crop_id= $getMasterSrvcDuration->dur_crop_id;
    		$dur_period= $getMasterSrvcDuration->dur_period;
        }else{
        	return response()->json(['status'=>false, 'status_code'=>401,'message'=>'Invalid Service Duration ID'], 400);
        }

        if($dur_price_ori >= 1){

	    	$api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
			$order  = $api->order->create([
					  //'receipt'         => 'order_rcptid_11',
					  'amount'          => $dur_price_ori*100, // amount in the smallest currency unit
					  'currency'        => 'INR',// <a href="/docs/payment-gateway/payments/international-payments/#supported-currencies" target="_blank">See the list of supported currencies</a>.)
					]);

			if(sizeof($order)>0){

				return response()->json(['status'=>true, 'order_id_status'=> true, 'status_code'=>200,'message'=>'Razor Pay Order ID Generated', 'order'=>$order->id]); 
			}else{
				return response()->json(['status'=>false, 'status_code'=>401,'message'=>'Failed to Generate Order ID'], 400);
			}
		}else{
			return response()->json(['status'=>true, 'order_id_status'=> false, 'status_code'=>200,'message'=>'Razor Pay Order ID Not Required']); 
		}

		//echo"<pre>";print_r($order); exit;

    }

    public function saveSubscription(Request $requests){

    	$usr_id = $requests->user()->usr_id;
    	$input = $requests->all();

        $messages = [
            'dur_id.required' => 'Duration ID Field is Required',
            ];

        $validator = Validator::make($input, [
            'dur_id' => 'required|numeric',
            'dur_price_ori' => 'required',
            'discount' => 'required',
            // 'razorpay_order_id' => 'required',
            // 'razorpay_payment_id' => 'required',
            // 'razorpay_signature' => 'required',
            
        ], $messages);
    
        if ($validator->fails()) {
            return response()->json(['status'=>false, 'status_code'=>401,'message'=>$validator->messages()->first()], 400);
        }

    	$dur_id= $requests->dur_id;
    	$dur_price_ori= $requests->dur_price_ori;
    	$discount= $requests->discount;

    	$razorpay_order_id= $requests->razorpay_order_id;
    	$razorpay_payment_id= $requests->razorpay_payment_id;
    	$razorpay_signature= $requests->razorpay_signature;

    	$dur_crop_id= 0;
    	$dur_period= 0;
    	$dur_period_days= 0;

    	$getMasterSrvcDuration = Master_srvcduration::where('dur_id', $dur_id)
								->first();
    				

        if(sizeof($getMasterSrvcDuration)>0){
    		$dur_crop_id= $getMasterSrvcDuration->dur_crop_id;
    		$dur_period= $getMasterSrvcDuration->dur_period;
    		$dur_period_days= $getMasterSrvcDuration->dur_period_days;
        }else{
        	return response()->json(['status'=>false, 'status_code'=>401,'message'=>'Invalid Service Duration ID'], 400);
        }


    	$ordData = array(
			'ord_id' => '',
			'ord_usr_id' => $usr_id,
			'ord_number' => '',
			'ord_subtotal' => $dur_price_ori+$discount,
			'ord_discount' => $discount,
			'ord_tax' => 0.00,
			'ord_total' => $dur_price_ori,
			'ord_currency'=> 'INR',
			'ord_date' => date("Y-m-d h:i:s"),
			'ord_status' => 'i',
			'payment_status' => '1'
		);

		DB::table('fn_user_orders')->insert($ordData);
		$inserted_order_id = DB::getPdo()->lastInsertId();

		$userGetScout = $requests->user()->userGetScout;

		if(sizeof($userGetScout)>0){
			$scout_id= $requests->user()->userGetScout->id;

			// scout -user - transaction
			$ord_sc_usrData = array(
				'user_id' => $usr_id,
				'scout_id' => $scout_id,
				'order_id' => $inserted_order_id
			);
			//DB::table('fn_user_scout_transaction')->insert($ord_sc_usrData);
			//
		}

		$ord_number = Helper::generateOrderNumber($usr_id, $inserted_order_id);


		$ordDetls = array(
                //'ord_det_id' => '',
                'ord_id' => $inserted_order_id,
                'ord_user' => $usr_id,
                'ord_loct' => $requests->user()->userLocation->loc_id,
                'ord_crop' => $dur_crop_id,
                'ord_crop_durtn' => $dur_period
        );
        DB::table('fn_user_orderdetails')->insert($ordDetls);


        $update_user_order = UserOrders::where('ord_id', $inserted_order_id)->update([
	                'ord_number' => $ord_number,
	                'ord_status' => 'a',
	                'payment_status' => '1'
	            ]);


    	//----- Generate Invoice Number
	    $invoiceNum= Helper::generateInvoiceNumber($usr_id, $inserted_order_id);

	    //----- Add To Invoice Table	
        $invoiceDetls = array(
            'inv_id' => '',
            'inv_number' => $invoiceNum,
            'inv_user_id' => $usr_id,
            'inv_order_id' => $inserted_order_id,
            'inv_amount' => $dur_price_ori,
            'inv_payment_status' => 'p',
            'inv_payment_time' => date("Y-m-d h:i:s"),
            'inv_gen_time' => date("Y-m-d h:i:s"),
            'file_name' => date('Y_m_d_H_i_s').".pdf",
        );
        DB::table('fn_user_invoices')->insert($invoiceDetls);
        $inserted_inv_id = DB::getPdo()->lastInsertId();


        // fn_user_srvcaccess

        $usr_loc_id = 0;
    	if(sizeof($requests->user()->userLocation)>0){
    		$usr_loc_id = $requests->user()->userLocation->loc_id;
    	}

    	$acc_start= date("Y-m-d");
    	$acc_end = date("Y-m-d", strtotime("+".$dur_period_days." days"));

        $serviceDetls = array(
		    'acc_id' => '',
		    'acc_usr_id' => $usr_id,
		    'acc_loctn_id' => $usr_loc_id,
		    'acc_crop_id' => $dur_crop_id,
		    'acc_invc_id' => $inserted_inv_id,
		    'acc_start' => $acc_start,
		    'acc_end' => $acc_end,
		    'acc_status' => 'a'
		);
		DB::table('fn_user_srvcaccess')->insert($serviceDetls);

		 $update_user_order = UserPlots::where(['plt_usr_id'=> $usr_id, 'plt_status' => 'a'])->update([
	                'plt_active_from' => $acc_start,
	                'plt_active_to' => $acc_end,
	            ]);

		if($dur_price_ori >= 1){
			$fn_order_razorpay = array(
			    'order_id' => $inserted_order_id,
			    'razorpay_order_id' => $razorpay_order_id,
			    'razorpay_payment_id' => $razorpay_payment_id,
			    'razorpay_signature' => $razorpay_signature,
			    'status' => 'P'
			);
			DB::table('fn_order_razorpay')->insert($fn_order_razorpay);

		}


		$update_user_step = User_step::where('stp_user_id', $usr_id)->update([
		                'stp_service' => 'y',
		                'stp_complete' => 'y',
		            ]);


        return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Successfull', 'order'=>$invoiceNum]);

    }


    public function updateUserLocation(Request $requests)
    {
    	$input = $requests->all();
    	$messages = [
            'lat.required' => 'Please Fill Lat',
            'long.required' => 'Please Fill Long',
            'loct_name.required' => 'Please Fill Loc Name',
            ];

        $validator = Validator::make($input, [
            'lat' => 'required|numeric',
            'long' => 'required|numeric', 
            'loct_name' => 'required',
        ], $messages);
    
        if ($validator->fails()) {
            return response()->json(['status'=>false, 'status_code'=>401,'message'=>$validator->messages()->first()], 400);
        }

        $loct_name = $requests->loct_name;
        $lat = $requests->lat;
        $lng = $requests->long;

        $geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&sensor=false&key=AIzaSyDTjIr8qMgE64AXk6Lazs1DgMt0juQBS7k');

        $output= json_decode($geocode);

        //
        $searchSql = DB::select("SELECT forecast_id, (3959*acos(cos(radians(".$lat."))*cos(radians(lattitude))*cos(radians(longitude)-radians(".$lng."))+sin(radians(".$lat."))*sin(radians(lattitude)))) AS distance FROM `fn_master_forecast` HAVING distance < 8 ORDER BY distance LIMIT 0,1");

        $forecast_id= 0;
        
        if(sizeof($searchSql)>0){
            $forecast_id= $searchSql[0]->forecast_id;
            
        }else{
            $for_max_id= 0;
            $for_max_id = DB::table('fn_master_forecast')->max('forecast_id');
            $forecast_id= $for_max_id+1;

            if($for_max_id){
                $forcMasterData = array(
                        'location_name' => @preg_replace("/[^a-zA-Z]+/", " ", $loct_name),
                        'forecast_id' => $for_max_id+1,
                        'dm_id' => '',
                        'aws_id' => '',
                        'wrf_loc_name' => '',
                        'lattitude' => $lat,
                        'longitude' => $lng,
                        'status' => 'i'
                );
                DB::table('fn_master_forecast')->insert($forcMasterData);
                $inserted_forecast_id = DB::getPdo()->lastInsertId();

                if($inserted_forecast_id){
                     $update_MasterForecast = Master_forecast::where('id', $inserted_forecast_id)->update([
                        'wrf_loc_name'=>$this->generateWRFName($inserted_forecast_id, $loct_name, 20)
                    ]);

                }
            }
        }

        $inserted_user_id = $requests->user()->usr_id;

        if(sizeof($requests->user()->userLocation)>0){

	        if($requests->user()->userLocation->change_loc_no==0){
	        	$update_MasterForecast = UserLocation::where('usr_id', $inserted_user_id)->update([
	                'forecast_id'=> $forecast_id,
	                'loc_name'=> $loct_name,
	                'loc_lat'=> $lat,
	                'loc_lng'=> $lng,
	                'change_loc_no' => '1'
	            ]);

	            return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Successfully Location Changed']);
	        }else{
	        	return response()->json(['status'=>false, 'status_code'=>401,'message'=>'Location has been changed once.'], 400);
	        }
	    }else{
	    	return response()->json(['status'=>false, 'status_code'=>401,'message'=>'Location Not Added Previously'], 400);
	    }
        //
        

    }

    public function generateWRFName($id=0, $name='', $length=20){
        $name = @preg_replace("/[^a-zA-Z]+/", "", $name);
        $name = $id."_".$name;
        $name = @mb_substr($name, 0, 20);
        return $name;
    }
	
	public function updateQuickBlox(Request $requests)
    {
    	
        $input = $requests->all();
		$usr_data= $requests->user();
        $user_id=$usr_data->usr_id;
		$user_type=$usr_data->usr_type_id;
		$usr_id = $usr_data->usr_id;
		// $chk=QuickBlox::where('user_id',$usr_id)->where('user_type',$user_type)->get();
		// foreach($chk as $key=>$v)
		// {
			// $quickbloxid=$v->quick_blox_id;
		// }
		if(!empty($requests->quick_blox_id))
		{
			$vv=QuickBlox::where('user_id',$usr_id)->where('user_type',$user_type)->update(['quick_blox_id'=>$requests->quick_blox_id ]);
		}
		

       return response()->json(['status'=>true, 'status_code'=>200,'message'=>'QuickBlox update successfully']); 
    }
	
	public function updateUserLanguage(Request $requests)
    {
    	
        $input = $requests->all();
		$usr_data= $requests->user();
        $user_id=$usr_data->usr_id;
		$user_type=$usr_data->usr_type_id;
		$usr_id = $usr_data->usr_id;
		// $chk=QuickBlox::where('user_id',$usr_id)->where('user_type',$user_type)->get();
		// foreach($chk as $key=>$v)
		// {
			// $quickbloxid=$v->quick_blox_id;
		// }
		if(!empty($requests->language))
		{
			$vv=UserGeneralInfo::where('gen_usr_id',$usr_id)->where('gen_usr_type_id',$user_type)->update(['language'=>$requests->language ]);
		}
		

       return response()->json(['status'=>true, 'status_code'=>200,'message'=>'Language update successfully']); 
    }
	
}
